#include "stdafx.h"
#include "Empresa.h"
#include <iostream>
#include <string>

using namespace std;
Empresa::Empresa()
{
}

Empresa::Empresa(string nome, string endereco, string cidade, string estado, string cep, string telefone)
{
	this->nome = nome;
	this->endereco = endereco;
	this->cidade = cidade;
	this->estado = estado;
	this->cep = cep;
	this->telefone = telefone;
}

string Empresa::getNome()
{
		return this->nome;
}

string Empresa::getEndereco()
{
	return this->endereco;
}

string Empresa::getCidade()
{
	return this->cidade;
}

string Empresa::getEstado()
{
	return this->estado;
}

string Empresa::getCep()
{
	return this->cep;
}

string Empresa::getTelefone()
{
	return this->telefone;
}

void Empresa::setNome(string nome)
{
	this->nome = nome;
}

void Empresa::setEndereco(string endereco)
{
	this->endereco = endereco;
}

void Empresa::setCidade(string cidade)
{
	this->cidade = cidade;
}

void Empresa::setEstado(string estado)
{
	this->estado = estado;
}

void Empresa::setCep(string cep)
{
	this->cep = cep;
}

void Empresa::setTelefone(string telefone)
{
	this->telefone = telefone;
}

void Empresa::imprime()
{
	cout << "Informacoes" << endl;
	cout << "Nome: " << nome << endl;
	cout << "Endereco: " << endereco << endl;
	cout << "Cidade: " << cidade << endl;
	cout << "Estado: " << estado << endl;
	cout << "CEP: " << cep << endl;
	cout << "Telefone: " << telefone << endl;
}


Empresa::~Empresa()
{
}
