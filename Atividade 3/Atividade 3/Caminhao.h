#pragma once
#include "Veiculo.h"

class Caminhao:public Veiculo
{
private:
	int cargaMax;
public:
	Caminhao();
	void imprimirCaminhao();
	int getCargaMax();
	void setCargaMax(int carga);
	~Caminhao();
};

