#include "stdafx.h"
#include "Restaurante.h"
#include <iostream>
#include <string>

using namespace std;
Restaurante::Restaurante()
{
}

Restaurante::Restaurante(string nome, string precoMedio, string tipoComida)
{
	setNome(nome);
	this->precoMedio = precoMedio;
	this->tipoComida = tipoComida;
}

void Restaurante::setTipoComida(string tipoComida)
{
	this->tipoComida = tipoComida;
}

void Restaurante::setPrecoMedio(string precoMedio)
{
	this->precoMedio = precoMedio;
}

string Restaurante::getTipoComida()
{
	return tipoComida;
}

string Restaurante::getPrecoMedio()
{
	return precoMedio;
}

void Restaurante::imprimeRestaurante()
{
	cout << "Restaurante Informacoes " << endl;
	cout << "Nome: " << getNome() << endl;
	cout << "Tipo Comida: " << tipoComida << endl;
	cout << "Preco medio: " << precoMedio << endl;

}


Restaurante::~Restaurante()
{
}
