#pragma once
#include "Veiculo.h"
#include <iostream>
#include <string>

using namespace std;
class CarroPasseio:public Veiculo
{
private:
	string cor = "";
	string modelo = "";

public:
	CarroPasseio();
	CarroPasseio(int peso, int velocidadeMax, float preco, string cor, string modelo);
	void setCor(string cor);
	void setModelo(string modelo);
	string getCor();
	string setModelo();
	void imprimir();
	~CarroPasseio();
};

