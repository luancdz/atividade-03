#include "stdafx.h"
#include "CarroPasseio.h"


CarroPasseio::CarroPasseio()
{
}

CarroPasseio::CarroPasseio(int peso, int velocidadeMax, float preco, string cor, string modelo)
{
	this->setPeso(peso);
	this->setVelocidadeMax(velocidadeMax);
	this->setPreco(preco);
	this->cor = cor;
	this->modelo = modelo;
}

void CarroPasseio::setCor(string cor)
{
	this->cor = cor;
}

void CarroPasseio::setModelo(string modelo)
{
	this->modelo = modelo;
}

string CarroPasseio::getCor()
{
	return this->cor;
}

string CarroPasseio::setModelo()
{
	return this->modelo;
}

void CarroPasseio::imprimir()
{
	cout << "Info Veiculo " << endl;
	cout << "Peso: " << getPeso() << endl;
	cout << "Velocidade Maxima: " << getVelocidadeMax() << endl;
	cout << "Preco: " << getPreco() << endl;
	cout << "Cor: " << this->cor << endl;
	cout << "Modelo: " << this->modelo << endl;
}


CarroPasseio::~CarroPasseio()
{
}
