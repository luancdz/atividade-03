#pragma once
#include "Empresa.h"


class Restaurante:public Empresa
{
private: 
	string tipoComida;
	string precoMedio;

public:
	Restaurante();
	Restaurante(string nome, string precoMedio, string tipoComida);
	void setTipoComida(string tipoComida);
	void setPrecoMedio(string precoMedio);
	string getTipoComida();
	string getPrecoMedio();
	void imprimeRestaurante();

	~Restaurante();
};

