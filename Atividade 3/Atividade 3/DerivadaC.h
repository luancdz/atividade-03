#pragma once
#include <iostream>
#include <string>
#include "DerivadaB.h"
#include "DerivadaA.h"

using namespace std;
class DerivadaC: public DerivadaA, public DerivadaB
{
public:
	DerivadaC();
	void DerivadaC::imprime();
	~DerivadaC();
};

