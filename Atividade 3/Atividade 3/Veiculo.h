#pragma once
class Veiculo
{
private:
	int peso = 0;
	int velocidadeMax = 0;
	float preco = 0;

public:
	Veiculo();
	Veiculo(int peso, int velocidadeMax, float preco);
	void imprimir();
	void setPeso(int peso);
	void setVelocidadeMax(int velocidadeMax);
	void setPreco(float preco);
	int getPeso();
	int getVelocidadeMax();
	float getPreco();
	~Veiculo();
};

