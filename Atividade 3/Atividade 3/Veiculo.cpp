#include "stdafx.h"
#include "Veiculo.h"
#include <iostream>
#include <string>

using namespace std;
Veiculo::Veiculo()
{
}

Veiculo::Veiculo(int peso, int velocidadeMax, float preco)
{
	this->peso = peso;
	this->velocidadeMax = velocidadeMax;
	this->preco = preco;
}

void Veiculo::imprimir()
{
	cout << "Info Veiculo " << endl;
	cout << "Peso: " << peso << endl;
	cout << "Velocidade Maxima" << velocidadeMax << endl;
	cout << "Preco: " << preco << endl;
}

void Veiculo::setPeso(int peso)
{
	this->peso = peso;

}

void Veiculo::setVelocidadeMax(int velocidadeMax)
{
	this->velocidadeMax = velocidadeMax;
}

void Veiculo::setPreco(float preco)
{
	this->preco = preco;
}

int Veiculo::getPeso()
{
	return this->peso;
}

int Veiculo::getVelocidadeMax()
{
	return this->velocidadeMax;
}

float Veiculo::getPreco()
{
	return this->preco;
}


Veiculo::~Veiculo()
{
}
