// Atividade 3.cpp : Define o ponto de entrada para a aplica��o de console.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include "Restaurante.h"
#include "Empresa.h"
#include "Veiculo.h"
#include "CarroPasseio.h"
#include "Caminhao.h"

using namespace std;
int main()
{
	// TESTE RESTAURANTE
	int tipoDeTeste;
	
	do {
		cout << "Se deseja testar a classe Resturante e suas derivadas pressione 1 " << endl;
		cout << "Se deseja testar a classe Veiculo e suas derivadas pressione 2 " << endl;
		cin >> tipoDeTeste;
	} while ((tipoDeTeste != 1) && (tipoDeTeste != 2));

	if (tipoDeTeste == 1) {
		Restaurante* restaurante = new Restaurante();
		restaurante->setNome("Mc Donalds");
		restaurante->setTipoComida("Fast food");
		restaurante->setPrecoMedio("20,00");
		restaurante->imprimeRestaurante();
		delete (restaurante);
	}
	if (tipoDeTeste == 2) {
		CarroPasseio* carroPasseio = new CarroPasseio(1500, 200, 30.000, "BORDO", "DEL REY");
		CarroPasseio* carroPasseioAux = new CarroPasseio();
		carroPasseio->imprimir();
		carroPasseioAux->setCor("AZUL");
		carroPasseioAux->setModelo("BELINA");
		carroPasseioAux->setPeso(5000);
		carroPasseioAux->setPreco(1.300);
		carroPasseioAux->setVelocidadeMax(80);
		carroPasseioAux->imprimir();

		cout << "Terminado de Imprimir os Carros de passeio ..." << endl;

		cout << "Imprimindo os caminhoes" << endl;
		delete(carroPasseio);
		delete(carroPasseioAux);

		Veiculo* caminhao = new Veiculo(25000, 140, 120.000);
		Caminhao* caminhaoAux = new Caminhao();
		caminhaoAux->setPeso(60000);
		caminhaoAux->setCargaMax(44600);
		caminhaoAux->setVelocidadeMax(50);
		caminhaoAux->setPreco(250.000);
		caminhao->imprimir();

		delete(caminhao);
		delete(caminhaoAux);
	}
	system("PAUSE");
    return 0;
}

