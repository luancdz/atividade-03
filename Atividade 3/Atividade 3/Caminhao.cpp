#include "stdafx.h"
#include "Caminhao.h"
#include <iostream>
#include <string>

using namespace std;

Caminhao::Caminhao()
{
}

void Caminhao::imprimirCaminhao()
{
	cout << "Caminhao info" << endl;
	cout << "Peso: " << getPeso() << endl;
	cout << "Velocidade Maxima" << getVelocidadeMax() << endl;
	cout << "Preco: " << getPreco() << endl;
	cout << "Carga Maxima: " << this->cargaMax << endl;
}

int Caminhao::getCargaMax()
{
	return this->cargaMax;
}

void Caminhao::setCargaMax(int carga)
{
	this->cargaMax = carga;
}


Caminhao::~Caminhao()
{
}
